# DerivationTree

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/vincenzopalazzo/derivationtree/master?style=flat-square)

<div align="center">
  <img src="https://gitlab.com/vincenzopalazzo/derivationtree/-/raw/master/icons/res/mipmap-xxxhdpi/ic_launcher.png" width="150" height="150"/>
</div>

> If you think like a computer, write C language make sense”
>                                    - Linus Torvalds, Nothing better than C

## Table of Content

- Introductions
- How to Use It
- Design Choice
- Build with
- Examples
- Future Work
- License

## Introductions

DerivationTree is a prolog software to make an interpretation of IMP language and return the derivation tree of the program.

## How to use it

The program is developed with [Swi-Prolog](https://www.swi-prolog.org/) and a couple of libraries described in the Build with section.

A make file is provided to help the user to run the program without lost a lot of time in the running command. The target example is reported below

- `make NAME_FILE=tests/code/while6.imp MEM="[(x, 3)]"`: This target takes the name of the file and the memory and return a latex file in the gen directory.
- `make check`: Run the simple and not complete prolog tests.
- `make check-all`: Run all the tests (included the integration testing).

#### Make options

Is possible to pass the name of the file from the make file. In fact, it is possible to use the option `NAME_FILE={path of file}`, and another important option in the make file is the possibility to the memory as a list of tuple `MEM=[(NAME_VAR, VALUE), (NAME_VAR, VALUE), ...]`. The list of a tuple is converted by the engine
in a dictionary where the key is the NAME_VAR and the VALUE is basically the value in memory of the variable.

In addition, for the moment the make file has a option to formatting the size of the PDF generated from latex, that is the `FILE_WIDTH={INT_VALUE}`.

#### Compile the latex File.

When the user runs the interpreter on an IMP program, the software will generate a latex file called `gen.tex` in the directory called gen, in the root of the directory. 

The latex file is tested with the `pdflatex` compiler, in particular with the
following command 

`pdflatex gen/gen.tex`

It will generate a pdf file called `gen.pdf` in the same directory where the `pdflatex` is run.

## Design choice

One of the main difficulties to write prolog code is how chose the correct prefix of the file, pl? prolog? or pro? [See here for the standard rules](https://www.swi-prolog.org/pldoc/man?section=fileext)

For this project is chosen the extension .prolog, because is explicative, and the more important fact is that this prefix avoids ambiguity between other languages like Perl.

In addition, the code is developed to be modular (or at least, it try to be modular), which means that is possible to write additional pieces to add more functionality, such as a web app or another type of persistence format. The main focus was to make the program more readable, and avoid all the strange things of prolog syntax, like bang operator ! or nested or with a lot of brackets.

## Build with

- [log4p](https://github.com/hargettp/log4p): Library to enable modular logging, but it contains some error with the fact when a custom atom is printed, for this reason, there is some write call in the program. With hope that we can update the package to the new version without this error.
- [tokenize ](https://github.com/aBathologist/tokenize): Library used to tokenize the file in different atoms, also this package has some bugs if the file is bad formatted or contains white/empty space/line at the end of the file. However, with a hack in the code, we are able to work around some of these wrong behaviors.

To install the packages is possible to run the following command in the Swi-Prolog interface or visit the official page.

```bash
?- pack_install(log4p).
?- pack_install(tokenize).
```

In addition, it is available the following make target

```bash
make dep
```

P.S: If the IMP file contains some wrong empty line at the end of it, the parser can return some error, with Emacs file format this behavior is not present, but exist with Vim and maybe with all the UI editor.

## IMP Examples

All the examples found in this repository is full based on the Book [Models of Computation](https://www.springer.com/it/book/9783319428987).

```c
x := 0;
while 0 <= y do (
      x := x + (2 * y) + 1 ;
      y := y - 1
)
```

With a empty memory the derivation tree has the following form

![](https://i.ibb.co/G2M2gth/Selection-070.png)

The real pdf is inside the gen directory of the repository.

To proof to myself that the program end with the correct memory, I try to proof that this IMP program is equal to the following dart program

```dart
void main() {

  var x = 0;
  var y = 0;
  while(0 <= y) {
    x = x + ( 2 *  y) + 1;
    y = y - 1; 
  }
  print(x);
  print(y);
}
```

You can try this dart code on the [DartPad](https://dartpad.dev/?null_safety=true)

# Future Work

For future work, it will be a cool idea to add some logic to divide the derivation tree in different places, like the while statment. In addition
another feature where I start to work on is the web View written in Javascript and Go language from a server-side where the Web view can require
to make the derivation tree to the server and the server will return the latex form of the derivation tree available for the web.

The work of the derivation tree IDE is available in a separate repository at the [following link](https://gitlab.com/vincenzopalazzo/derivationtreeide).

All the code of the Web IDE is under the same Open Source license.

## License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

DerivationTree software developed with the University of Pisa for
courses Principles for Software Composition 
http://didawiki.di.unipi.it/doku.php/magistraleinformatica/psc/start.

Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the class Principles for Software Composition
http://didawiki.di.unipi.it/doku.php/magistraleinformatica/psc/start

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
