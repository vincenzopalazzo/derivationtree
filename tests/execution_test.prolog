:- begin_tests(lists).
:- use_module(library(lists)).

:- consult('../src/engine/inference_rules').
:- reconsult('../src/engine/symbol_table').

test(assig_val, [true(Val == 1)]) :-
    init_mem(Init),
    init_mem(Init, [("x", 2)], StartMem),
    inf_commands([assign(var(x),decrement(var(x),number(1)))], _, _,
                 StartMem, FinalMem),
    val_in_mem(FinalMem, "x", Val).

test(test_while, [true(Val == 0)]) :-
    init_mem(Init),
    init_mem(Init, [], StartMem),
    inf_commands([while(gt(var(x),number(1)),
                       [assign(var(x),decrement(var(x),number(1)))])],
                 _, _, StartMem, FinalMem),
    val_in_mem(FinalMem, "x", Val).

test(test_while_iter, [true(Val =@= 1)]) :-
    init_mem(Init),
    init_mem(Init, [("x", 12)], StartMem),
    inf_commands([while(gt(var(x),number(1)),
                       [assign(var(x),decrement(var(x),number(1)))])],
                 _, _, StartMem, FinalMem),
    val_in_mem(FinalMem, "x", Val).

:- end_tests(lists).
