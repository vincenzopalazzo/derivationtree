% Prolog test of the lexer module

:- begin_tests(lists).
:- use_module(library(lists)).
:- consult('../src/lexer/lexer').

test(lexingTokenOne, [true(LexemeList =@= [])]) :-
    scanner([], LexemeList).

test(lexingTokenTwo, [true(LexemeList =@= ['WHILE', 'LEFT_PAR', 'ID', 'GT', 'DIGIT', 'RIGHT_PAR', 'DO', 'LEFT_PAR', 'ID', 'ASSIGN', 'DIGIT', 'RIGHT_PAR'])]) :-
    scanner(['while', '(', 'x', '>', '0', ')', 'do', '(', 'x', ':=', '1', ')' ], LexemeList).

test(lexingTokenWithForLikeID, [true(LexemeList =@= ['ID', 'LEFT_PAR', 'ID', 'GT', 'DIGIT', 'RIGHT_PAR', 'DO', 'LEFT_PAR', 'ID', 'ASSIGN', 'DIGIT', 'RIGHT_PAR'])]) :-
    scanner(['for', '(', 'x', '>', '0', ')', 'do', '(', 'x', ':=', '1', ')' ], LexemeList).

:- end_tests(lists).
