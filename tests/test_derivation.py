#!/usr/bin/env python3

import subprocess
import os
import glob
import logging
import pytest


logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(message)s", "%m-%d-%Y %H:%M:%S"
)

def test_prolog_derivation_success():
    """
    TODO: adding test
    """
    count_tests = 0
    for filepath in glob.iglob("tests/code/*.imp"):
        count_tests = count_tests + 1
        output = subprocess.call(["make", "NAME_FILE={}".format(filepath), "MEM={}".format("[(x, 2)]")])
        assert output is 0, "T: {} Error on file {}".format(count_tests, filepath)
    logging.info("Complete with success {} derivation".format(count_tests))

