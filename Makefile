COMPILER=swipl

NAME_FILE=

MEM=

FILE_WIDTH=200

default:
	$(COMPILER) -g "imp('$(NAME_FILE)', $(MEM), $(FILE_WIDTH))" -t halt src/main.prolog

dep:
	$(COMPILER) -g "pack_install(tokenize, [interactive(false)])." -t halt
	$(COMPILER) -g "pack_install(log4p, [interactive(false)])." -t halt

debug:
	$(COMPILER) -g "debug, imp('$(NAME_FILE)', [("x", 1), ("y", 1)], $(FILE_WIDTH))" -t halt src/main.prolog

check:
	$(COMPILER) -g run_tests -t halt tests/main.prolog

check-all: check
	pytest .
