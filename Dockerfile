FROM ubuntu
LABEL mantainer="Vincenzo Palazzo vincenzopalazzodev@gmail.com"

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:swi-prolog/stable
RUN apt-get update
RUN apt-get install -y swi-prolog
RUN apt-get install -y python3 python3-pip make

RUN pip install pytest

WORKDIR /code

#COPY . .

CMD ["./entrypoint.sh"]