% This file contains the implementation of the parser for the IMP language,
% this file take the list of token generated from the scanner and return
% the AST of the language.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

% As a imperative language, the language is defined
% as a list of statments
:- use_module(library(log4p)).
:- table a_expr//1. % Use tabling to remove the left recursion for the a_expr clause

% parse/2
% The horn clause parse contains the logic to run the
% analysis over the list of tokens
parse(ListTokens, ListStmt) :-
    phrase(commands(ListStmt), ListTokens).

% command/1
% this horn clause is used to detect all the type of commands
% valid in a IMP program
commands([]) --> [].

commands([Command]) --> command(Command).

commands(Commands) --> [punct("(")], commands(Commands), [punct(")")], {debug("Command between ()")}.

commands([Command | Commands]) --> command(Command), [punct(";")], commands(Commands).

command(Command) --> [punct("(")], command(Command), [punct(")")].

command(Skip) --> [keyword("skip")], {Skip = skip(), debug("Found skip")}.

command(Assign) --> a_expr(X), [punct(":")], [punct("=")], a_expr(Y),
                    {Assign = assign(X, Y)}.

command(While) --> [keyword("while")], b_expr(X), [keyword("do")], commands(Commands),
                   {While = while(X, Commands), debug("While found")}.

command(If) --> [keyword("if")], b_expr(X), [keyword("then")],
                commands(C0s), [keyword("else")], commands(C1s),
                {If = if(X, C0s, C1s)}.

% a_expr/1
% This horn clause is used detect the arithmetic expression
% valid in a IMP program
a_expr(Expr) --> [punct("(")], a_expr(Expr), [punct(")")], {debug("Aexpr between ()"), write(Expr)}.

a_expr(Var)   --> [word(Name)], {Var = var(Name), debug("Found Variable "), debug(Name)}.

a_expr(Val)   --> [number(X)], {Val = number(X), debug("found number")}.

a_expr(Expr)  --> a_expr(X), [punct("+")], a_expr(Y),
                 {Expr = increment(X, Y), debug("Found increment")}.

a_expr(Expr)  --> a_expr(X), [punct("-")], a_expr(Y), {Expr = decrement(X, Y)}.

a_expr(Expr)  --> a_expr(X), [punct("*")], a_expr(Y), {Expr = multipl(X, Y)}.

% b_expr/1
% This horn clause it used to detect the binary expression
% valid in a IMP program
b_expr(True)  --> [keyword("true")], {True = bool(true)}.

b_expr(False) --> [keyword("false")], {False = bool(false)}.

b_expr(Cmp)   --> a_expr(X), [punct("=")], a_expr(Y), {Cmp = cmp(X, Y)}.

b_expr(Gt)    --> a_expr(X), [punct(">")], a_expr(Y), {Gt = gt(X, Y), debug("Found > op")}.

b_expr(GtEq)  --> a_expr(X), [punct(">")], [punct("=")], a_expr(Y), {GtEq = gt_eq(X, Y)}.

b_expr(Lt)    --> a_expr(X), [punct("<")], a_expr(Y), {Lt = lt(X, Y)}.

b_expr(LtEq)  --> a_expr(X), [punct("<")], [punct("=")], a_expr(Y), {LtEq = lt_eq(X, Y), debug("Found < op")}.

b_expr(NotEq)  --> a_expr(X), [punct("!")], [punct("=")], a_expr(Y), {NotEq = not_eq(X, Y)}.

b_expr(Not)  --> [punct("!")], a_expr(X), {Not = not(X)}.

b_expr(And)  --> a_expr(X), [punct("&")], [punct("&")], a_expr(Y), {And = and(X, Y)}.

b_expr(Or)  --> a_expr(X), [punct("|")], [punct("|")], a_expr(Y), {Or = or(X, Y)}.

b_expr(Expr)  --> [punct("(")], b_expr(Expr), [punct(")")],
                  {debug("Found expression bwn (expr)")}.
