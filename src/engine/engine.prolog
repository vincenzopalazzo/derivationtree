% Engine of the interpreter,
% this take in input the tokens and and the memory
% and return the derivation tree and the memory result.

:- consult('../lexer/lexer').
:- consult('../parser/parser').
:- consult('./inference_rules').
:- consult('../persistence/store').

imp(FileName, Mem, Width) :-
    scanner(FileName, LexemList),
    write(LexemList), nl,
    parse(LexemList, ListStmt),
    write(ListStmt), nl,
    infer(ListStmt, DerivationTree, Mem, FinalMem),
    write(FinalMem), nl,
    store("latex", DerivationTree, "gen/gen.tex", Width).
