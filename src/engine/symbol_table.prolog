% This File contains the implementation of the Symbol Table
% in particular the Symbol Table is used to Evaluate the
% inference rules in the memory MEM
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

% Developed with Hash Map. https://eu.swi-prolog.org/pldoc/man?section=bidicts
:- use_module(library(log4p)).

%init_mem/2
init_mem(MemToInit) :-
    MemToInit = mem{}.

% init_mem/2
% TODO: Add doc of horn clause
init_mem(Mem, [], NewMem) :-
    NewMem = Mem.

init_mem(Mem, [(VarName, Val) | List], NewMem) :-
    string_to_atom(VarName, AtomName),
    UpdateMem = Mem.put(AtomName, Val),
    init_mem(UpdateMem, List, NewMem).


% update_mem/3
% This horn clause is used to update the memory
% with the new value provided as touble (Name, Val).
update_mem(Mem, (VarName, Val), NewMem) :-
    string_to_atom(VarName, AtomName),
    NewMem = Mem.put(AtomName, Val).


% TODO: How we can change the logic here?
% by definition all the variable are set to 0 if they are not inizialized.
val_in_mem(Mem, VarName, Value) :-
    string_to_atom(VarName, AtomName),
    (Value = Mem.get(AtomName); Value is 0), !.
