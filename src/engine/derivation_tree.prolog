% This file include the tree data structure to
% implement the derivation tree from the inference rules.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
:- use_module(library(log4p)).

% The idea of this derivation tree is define to couple of method
% to define a new node with a list of derivation as result.
% this is a experimental idea and can have some corner case where it can not
% work, but without try we never know.

:- reconsult('./symbol_table').

init_derivation(InfRule, StartMem, TreeNode) :-
    init_mem(Placeholder),
    TreeNode = der(InfRule, [], StartMem, Placeholder), !.

add_node(der(RootDer, List, S, P), NewDerivation, UpdateNode) :-
    NewList = [NewDerivation | List],
    UpdateNode = der(RootDer, NewList, S, P), !.

add_mem(der(Root, Child, S, _), Mem, DerWithMem) :-
    DerWithMem = der(Root, Child, S, Mem), !.
