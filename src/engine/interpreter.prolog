% This file include the logic to evaluate the IMP
% expression of the program and return the result
% of the expression.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

:- use_module(library(log4p)).

:- reconsult('./symbol_table').

% eval/3
% The eval horn clause take in input the memory
% and the token to execute the evaluation.

eval_exp(Mem, var(Name), Result) :-
    debug("Eval variable"),
    val_in_mem(Mem, Name, Result).

eval_exp(_, number(Val), Result) :-
    debug("Eval number"),
    Result = Val.

eval_exp(Mem, increment(Expr1, Expr2), Result) :-
    debug("Eval increment"),
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    Result is (ResultOne + ResultTwo).

eval_exp(Mem, decrement(Expr1, Expr2), Result) :-
    debug("Eval decrement"),
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    Result is (ResultOne - ResultTwo).

eval_exp(Mem, multipl(Expr1, Expr2), Result) :-
    debug("Eval Multiply"),
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    Result is (ResultOne * ResultTwo).

eval_exp(_, bool(X), Result) :-
    Result = X.

eval_exp(Mem, cmp(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    Result = (ResultOne =:= ResultTwo).

eval_exp(Mem, gt(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    (ResultOne > ResultTwo, Result = true); Result = false.

eval_exp(Mem, gt_eq(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    (ResultOne >= ResultTwo, Result = true); Result = false.

eval_exp(Mem, lt(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    (ResultOne < ResultTwo, Result = true); Result = false.

eval_exp(Mem, lt_eq(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    write(Expr1), write(" vs "), write(Expr2),nl,
    write(ResultOne), write(" vs "), write(ResultTwo),nl,
    write(Mem),
    (ResultOne =< ResultTwo, Result = true); Result = false.

eval_exp(Mem, not_eq(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    (ResultOne =\= ResultTwo, Result = true); Result = false.

eval_exp(Mem, not(Expr1), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    ResultOne = true, Result = false; Result = true.

eval_exp(Mem, and(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    ResultOne = true, ResultTwo = true, Result = true; Result = false.

eval_exp(Mem, or(Expr1, Expr2), Result) :-
    eval_exp(Mem, Expr1, ResultOne),
    eval_exp(Mem, Expr2, ResultTwo),
    ResultOne = false, ResultTwo = false, Result = false; Result = true.
