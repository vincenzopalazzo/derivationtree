% This file include the inference rules of the IMP language
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
:- use_module(library(log4p)).

:- table inf_a_expr//3.

:- consult('./symbol_table').
:- consult('./interpreter').
:- consult('./derivation_tree').
:- consult('../utils/utilities').

infer(ListOfStmt, DerivationTree, InitMem, FinalMem) :-
    init_mem(EmptyMem),
    debug("********* init ***********"),
    init_mem(EmptyMem, InitMem, StartMem),
    init_derivation(program, StartMem, StartDerTree),
    write("Derivation "), write(StartDerTree), nl,
    write(StartMem), nl,
    debug("********** end ***********"),
    inf_commands(ListOfStmt, StartDerTree, DerWithMem, StartMem, FinalMem),
    add_mem(DerWithMem, FinalMem, DerivationTree),
    write("Finish with mem "), write(FinalMem), nl,
    write("Derivation Tree is "), write(DerivationTree).

% inf_commands/2
% This horn clause it is used to execute the statments in the list
% and modify the memory.
inf_commands([], StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference evaluation finished"),
    DerivationTree = StartDerTree,
    FinalMem = StartMem.

inf_commands([Command], StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule on a list of commands"),
    inf_command(Command, StartDerTree, DerivationTree, StartMem, FinalMem).

inf_commands([Command | Commands], StartDerTree, DerivationTree,
             StartMem, FinalMem) :-
    debug("Inference rule on a list of commands"),
    init_derivation(seq([Command | Commands]), StartMem, SeqDer),
    inf_command(Command, SeqDer, TmpDer, StartMem, TmpMem),
    inf_commands_seq(Commands, TmpDer, Tmp2Der, TmpMem, FinalMem),
    add_mem(Tmp2Der, FinalMem, SeqDerTree),
    add_node(StartDerTree, SeqDerTree, DerivationTree).

inf_commands_seq([Command | Commands], StartDerTree, DerivationTree,
                 StartMem, FinalMem) :-
    debug("Inference rule on a list of commands from seq"),
    inf_command(Command, StartDerTree, TmpDer, StartMem, TmpMem),
    inf_commands_seq(Commands, TmpDer, DerivationTree, TmpMem, FinalMem).

inf_commands_seq([], StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule on a list of commands from seq"),
    FinalMem = StartMem,
    DerivationTree = StartDerTree.

% inf_command/2
% TODO add documentation on this horn clause
inf_command(skip(), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule for ship command"),
    init_derivation(skip(), StartMem, SkipDer),
    FinalMem = StartMem,
    add_mem(SkipDer, FinalMem, SkipDerWithMem),
    add_node(StartDerTree, SkipDerWithMem, DerivationTree).

inf_command(assign(var(Name), YExpr), StartDerTree, DerivationTree, StartMem, FinalMem):-
    debug("Inference rule for assigment command"),
    init_derivation(assign(var(Name), YExpr), StartMem, AssignDer),
    eval_exp(StartMem, YExpr, ResultExpr),
    % inf_a_expr(var(Name), AssignDer, ToDerTree, StartMem, Tmp1Mem),
    inf_a_expr(YExpr, AssignDer, AssignDerTree, StartMem, Tmp2Mem),
    update_mem(Tmp2Mem, (Name, ResultExpr), FinalMem),
    add_mem(AssignDerTree, FinalMem, AssignWithMem),
    add_node(StartDerTree, AssignWithMem, DerivationTree).

inf_command(while(BOp, ListCommand), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule for while command"),
    init_derivation(while(BOp, ListCommand), StartMem, WhileDer),
    inf_b_expr(BOp, WhileDer, WhileDerBOp, StartMem, MemWithBOp),
    eval_exp(MemWithBOp, BOp, Result),
    (Result == true,
     inf_commands(ListCommand, WhileDerBOp, CycleDer, MemWithBOp, CycleMem),
     inf_command(while(BOp, ListCommand), CycleDer, CycleDerRes, CycleMem, FinalMem),
     ResDer = CycleDerRes
    ;
    (not_inst(CycleDerRes), ResDer = WhileDerBOp, FinalMem = StartMem; ResDer = CycleDerRes)
    %loop_entry(Result, BOp, ListCommand, WhileDerBOp, CycleDer, StartMem, FinalMem),
    ),
    add_mem(ResDer, FinalMem, WithMem),
    add_node(StartDerTree, WithMem, DerivationTree), !.


inf_command(if(BOp, ThenCommand, ElseCommand), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule for the if command"),
    init_derivation(if(BOp, ThenCommand, ElseCommand), StartMem, IfDerivation),
    inf_b_expr(BOp, IfDerivation, IfDerBOp, StartMem, TmpMem),
    eval_exp(TmpMem, BOp, Result),
    (Result == true,
     inf_commands(ThenCommand, IfDerBOp, TmpDer, TmpMem, FinalMem)
    ;
     inf_commands(ElseCommand, IfDerBOp, TmpDer, TmpMem, FinalMem)
    ),
    add_mem(TmpDer, FinalMem, DerWithMem),
    add_node(StartDerTree, DerWithMem, DerivationTree), !.

inf_a_expr(number(Val), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("inference rule found for num"),
    init_derivation(number(Val), StartMem, NumDer),
    FinalMem = StartMem,
    add_mem(NumDer, FinalMem, DerWithMem),
    add_node(StartDerTree, DerWithMem, DerivationTree), !.

inf_a_expr(var(Name), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("inference rule found for var"),
    init_derivation(var(Name), StartMem, VarDer),
    FinalMem = StartMem,
    add_mem(VarDer, FinalMem, VarDerWithMem),
    add_node(StartDerTree, VarDerWithMem, DerivationTree), !.

inf_a_expr(increment(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("inference rule found for the aexpr/increment"),
    init_derivation(increment(XExpr, YExpr), StartMem, SumDer),
    inf_a_expr(YExpr, SumDer, Tmp1Der, StartMem, Tmp1Mem),
    inf_a_expr(XExpr, Tmp1Der, Tmp2Der, Tmp1Mem, FinalMem),
    add_mem(Tmp2Der, FinalMem, SumDerWithMem),
    add_node(StartDerTree, SumDerWithMem, DerivationTree), !.

inf_a_expr(decrement(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("inference rule found for the aexpr/decrement"),
    init_derivation(decrement(XExpr, YExpr), StartMem, SumDer),
    inf_a_expr(YExpr, SumDer, Tmp1Der, StartMem, Tmp1Mem),
    inf_a_expr(XExpr, Tmp1Der, Tmp2Der, Tmp1Mem, FinalMem),
    add_mem(Tmp2Der, FinalMem, SumDerWithMem),
    add_node(StartDerTree, SumDerWithMem, DerivationTree), !.

inf_a_expr(multipl(XExpr, YExpr), StartDerTree, DerivationTree,
           StartMem, FinalMem) :-
    debug("inference rule found for the aexpr/decrement"),
    init_derivation(multipl(XExpr, YExpr), StartMem, MulDer),
    inf_a_expr(YExpr, MulDer, Tmp1Der, StartMem, Tmp1Mem),
    inf_a_expr(XExpr, Tmp1Der, Tmp2Der, Tmp1Mem, FinalMem),
    add_mem(Tmp2Der, FinalMem, MulDerWithMem),
    add_node(StartDerTree, MulDerWithMem, DerivationTree), !.

inf_b_expr(bool(Val), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("inference rule found for the bexpr/bool(true)"),
    init_derivation(bool(Val), StartMem, BoolDer),
    FinalMem = StartMem,
    add_mem(BoolDer, FinalMem, BoolDerWithMem),
    add_node(StartDerTree, BoolDerWithMem, DerivationTree), !.

inf_b_expr(cmp(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule found for the bexpr/cmp(X, Y)"),
    inf_a_expr(XExpr, StartDerTree, DerivationTree, StartMem, FinalMem),
    inf_a_expr(YExpr, StartDerTree, DerivationTree, StartMem, FinalMem), !.

inf_b_expr(gt(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule found for the bexpr/gt(X, Y)"),
    init_derivation(gt(XExpr, YExpr), StartMem, GtDer),
    inf_a_expr(XExpr, GtDer, Tmp1Der, StartMem, TmpMem),
    inf_a_expr(YExpr, Tmp1Der, Tmp2Der, TmpMem, FinalMem),
    add_mem(Tmp2Der, FinalMem, GtDerWithMem),
    add_node(StartDerTree, GtDerWithMem, DerivationTree), !.

inf_b_expr(gt_eq(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule found for the bexpr/gt_eq(X, Y)"),
    inf_a_expr(XExpr, StartDerTree, DerivationTree, StartMem, TmpMem),
    inf_a_expr(YExpr, StartDerTree, DerivationTree, TmpMem, FinalMem), !.

inf_b_expr(lt(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule found for the bexpr/lt(X, Y)"),
    inf_a_expr(XExpr, StartDerTree, DerivationTree, StartMem, FinalMem),
    inf_a_expr(YExpr, StartDerTree, DerivationTree, StartMem, FinalMem), !.

inf_b_expr(lt_eq(XExpr, YExpr), StartDerTree, DerivationTree, StartMem, FinalMem) :-
    debug("Inference rule found for the bexpr/lt_eq"),
    init_derivation(lt_eq(XExpr, YExpr), StartMem, LTEqDer),
    inf_a_expr(XExpr, LTEqDer, Tmp1Der, StartMem, TmpMem),
    inf_a_expr(YExpr, Tmp1Der, Tmp2Der, TmpMem, FinalMem),
    add_mem(Tmp2Der, FinalMem, LtEqDerWithMem),
    add_node(StartDerTree, LtEqDerWithMem, DerivationTree), !.
