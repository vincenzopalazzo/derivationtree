% This file include the implementation of the scanner for the IMP
% language, it use an external library to make the parsing of the file/string.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

% https://github.com/shonfeder/tokenize
:- use_module(library(tokenize)).
:- use_module(library(lists)).
:- use_module(library(apply)).

% scanner/2
% This horn clause is used to parse the file with the name FileName
% and return a list of Tokens where is possible make the parsing
% of the code.
scanner(FileName, TokenList) :-
    tokenize_file(FileName, LexemeList,
                  [cntrl(false), spaces(false), cased(true), to(strings)]),
    write(LexemeList), nl,
    % convert some special word into keyword
    convlist(append_as_key, LexemeList, WithSpace),
    % The library tokenize prude addition space, with the following line
    % we fixed this bug in the library.
    exclude(is_space, WithSpace, TokenList),
    write(TokenList), nl.

% is_space/1
% This is a function that is used in the filter operation
% to remove the space in list of tokens
% This is a hack to work around to an possible bug in the tokenize library
is_space(space(" ")).

% append_as_key/2
% It this a Horn clause to change some word in keyword to make more
% easy/readable the code and the parsing phase.
append_as_key(word("while"), Out) :-
    Out = keyword("while").

append_as_key(word("do"), Out) :-
    Out = keyword("do").

append_as_key(word("true"), Out) :-
    Out = keyword("true").

append_as_key(word("false"), Out) :-
    Out = keyword("false").

append_as_key(word("skip"), Out) :-
    Out = keyword("skip").

append_as_key(word("if"), Out) :-
    Out = keyword("if").

append_as_key(word("then"), Out) :-
    Out = keyword("then").

append_as_key(word("else"), Out) :-
    Out = keyword("else").

append_as_key(Any, Out) :-
    Out = Any.
