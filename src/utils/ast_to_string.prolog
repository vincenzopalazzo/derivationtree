% This file include the logic to convert the AST
% into a string.

ast_to_string_list([], StartStr, String) :-
    String = StartStr.

% TODO: Way it is not a list?
%ast_to_string_list(Command, StartStr, String) :-
%    ast_to_string(Command, CmdStr),
%    atomics_to_string([StartStr, "; ", CmdStr], String).

ast_to_string_list([Command | List], StartStr, String) :-
    ast_to_string(Command, CmdStr),
    (StartStr \= "", atomics_to_string([StartStr, "; ", CmdStr], TmpStr);
     atomics_to_string([CmdStr], TmpStr)
    ), ast_to_string_list(List, TmpStr, String).

ast_to_string_list([Command], StartStr, String) :-
        ast_to_string(Command, CmdStr),
        (StartStr \= "", atomics_to_string([StartStr, "; ", CmdStr], TmpStr);
         atomics_to_string([CmdStr], TmpStr)
        ), ast_to_string_list([], TmpStr, String).

ast_to_string_list(Command, StartStr, String) :-
    ast_to_string(Command, CmdStr),
    (StartStr \= "", atomics_to_string([StartStr, "; ", CmdStr], TmpStr);
     atomics_to_string([CmdStr], TmpStr)
    ), ast_to_string_list([], TmpStr, String).


ast_to_string_list(ListCommand, String) :-
    ast_to_string_list(ListCommand, "", String).

ast_to_string(if(BOp, IfStm, ElseStm), String) :-
    ast_to_string(BOp, BOpString),
    ast_to_string_list(IfStm, "", IfString),
    ast_to_string_list(ElseStm, "", ElseString),
    atomics_to_string(["if ( ", BOpString, " ) then ",
                  "( ", IfString, " ) else ( ", ElseString, " ) "], String).

ast_to_string(while(BOp, Stms), String) :-
    ast_to_string(BOp, BOpString),
    ast_to_string_list(Stms, "", StmString),
    atomics_to_string(["while (", BOpString, ") do (", StmString, ")"], String).

ast_to_string(assign(X, To), String) :-
    ast_to_string(X, VarString),
    ast_to_string(To, ToString),
    atomics_to_string([VarString," := ", ToString], String).

ast_to_string(decrement(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " - ", YString], String).

ast_to_string(increment(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " + ", YString], String).

ast_to_string(multipl(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " * ", YString], String).

ast_to_string(gt(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " > ", YString], String).

ast_to_string(gt_eq(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " >= ", YString], String).

ast_to_string(lt(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " < ", YString], String).

ast_to_string(lt_eq(X, Y), String) :-
    ast_to_string(X, XString),
    ast_to_string(Y, YString),
    atomics_to_string([XString, " <= ", YString], String).

ast_to_string(skip(), String) :-
    String = "skip()".

ast_to_string(bool(X), String) :-
    atomics_to_string([X], String).

ast_to_string(number(Val), String) :-
    number_string(Val, String).

ast_to_string(var(Val), String) :-
    String = Val.
