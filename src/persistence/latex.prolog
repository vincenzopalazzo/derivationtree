% This file contains the logic to store the
% derivation tree in a latex format.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

:- consult('../utils/ast_to_string').
:- reconsult('../engine/symbol_table').
:- reconsult('../engine/interpreter').

store_to_latex(Where, DerivationTree, Width) :-
    open(Where, write, LatexFile),
    write(LatexFile, "% auto generate file, please not modify the content\n"),
    write(LatexFile, "\\documentclass[10pt]{article}\n"),
    write(LatexFile, "\\usepackage{proof}\n"),
    write(LatexFile, "\\pdfpagewidth="),
    write(LatexFile, Width),
    write(LatexFile, "mm\n"),
    write(LatexFile, "\\pdfpageheight=150mm\n"),
    %write(LatexFile, "\\usepackage{lscape}\n"),
    write(LatexFile, "\\begin{document}\n"),
    %write(LatexFile, "\\begin{landscape}"),
    write(LatexFile, "\\centering"),
    write(LatexFile, "\\[\n"),
    tree_to_latex(DerivationTree, LatexFile),
    write(LatexFile, "\\]\n"),
    %write(LatexFile, "\\end{landscape}"),
    write(LatexFile, "\\end{document}\n"),
    close(LatexFile).

tree_to_latex(der(program, List, _, _), LatexFile) :-
    iterate_over_list(List, LatexFile).

tree_to_latex(der(seq(ListCommand), Child, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[seq]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string_list(ListCommand, CmdsStr),
    write(LatexFile, CmdsStr),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, ", "),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    write_mem_to_string(FinalMem, FinalMemStr),
    write(LatexFile, FinalMemStr),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Child, LatexFile),
    write(LatexFile, "\n}").

tree_to_latex(der(if(BOp, If, Else), DerChild, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[if]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(if(BOp, If, Else), IfString),
    write(LatexFile, IfString),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StringStartMem),
    write(LatexFile, StringStartMem),
    write(LatexFile, " \\rangle \\to "),
    write_mem_to_string(FinalMem, StringFinalMem),
    write(LatexFile, StringFinalMem),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(DerChild, LatexFile),
    write(LatexFile, "\n}").

tree_to_latex(der(while(BOp, Stmts), DerChild, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[while]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(while(BOp, Stmts), WhileString),
    write(LatexFile, WhileString),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StringStartMem),
    write(LatexFile, StringStartMem),
    write(LatexFile, " \\rangle \\to "),
    write_mem_to_string(FinalMem, StringFinalMem),
    write(LatexFile, StringFinalMem),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(DerChild, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(assign(X, Y), Children, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[assign]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(assign(X, Y), AssignString),
    write(LatexFile, AssignString),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    write_mem_to_string(FinalMem, FinalMemStr),
    write(LatexFile, FinalMemStr),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(increment(X, Y), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[sum]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(increment(X, Y), SumStr),
    write(LatexFile, SumStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, increment(X, Y), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(decrement(X, Y), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[dec]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(decrement(X, Y), SumStr),
    write(LatexFile, SumStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, decrement(X, Y), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(multipl(X, Y), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[mul]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(multipl(X, Y), SumStr),
    write(LatexFile, SumStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, multipl(X, Y), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(gt(XExpr, YExpr), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[gt]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(gt(XExpr, YExpr), GtStr),
    write(LatexFile, GtStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, gt(XExpr, YExpr), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(gt_eq(XExpr, YExpr), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[gt]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(gt_eq(XExpr, YExpr), GtStr),
    write(LatexFile, GtStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, gt_eq(XExpr, YExpr), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(lt(XExpr, YExpr), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[gt]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(lt(XExpr, YExpr), GtStr),
    write(LatexFile, GtStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, lt(XExpr, YExpr), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(lt_eq(XExpr, YExpr), Children, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[gt]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(lt_eq(XExpr, YExpr), GtStr),
    write(LatexFile, GtStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    eval_exp(StartMem, lt_eq(XExpr, YExpr), Result),
    write(LatexFile, Result),
    write(LatexFile, "}\n"),
    write(LatexFile, "{"),
    iterate_over_list(Children, LatexFile),
    write(LatexFile, "}").

tree_to_latex(der(number(X), _, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[num]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(number(X), NumString),
    write(LatexFile, NumString),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    write(LatexFile, NumString),
    write(LatexFile, "}\n"),
    write(LatexFile, "{}").

tree_to_latex(der(var(X), _, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[var]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(var(X), VarString),
    write(LatexFile, VarString),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    val_in_mem(FinalMem, X, Value),
    write(LatexFile, Value),
    write(LatexFile, "}"),
    write(LatexFile, "\n{}").

tree_to_latex(der(skip(), _, StartMem, FinalMem), LatexFile) :-
    write(LatexFile, "\\infer[skip]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(skip(), SkipStr),
    write(LatexFile, SkipStr),
    write(LatexFile, ", "),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to"),
    write_mem_to_string(FinalMem, FinalMemStr),
    write(LatexFile, FinalMemStr),
    write(LatexFile, "}"),
    write(LatexFile, "\n{}").

tree_to_latex(der(bool(Val), _, StartMem, _), LatexFile) :-
    write(LatexFile, "\\infer[bool]\n"),
    write(LatexFile, "{\\langle "),
    ast_to_string(bool(Val), BoolStr),
    write(LatexFile, BoolStr),
    write(LatexFile, ","),
    write_mem_to_string(StartMem, StartMemStr),
    write(LatexFile, StartMemStr),
    write(LatexFile, " \\rangle \\to "),
    write(LatexFile, BoolStr),
    write(LatexFile, "}"),
    write(LatexFile, "\n{}").

write_mem_to_string(Mem, String) :-
    term_string(Mem, MemString),
    re_replace("{", "[", MemString, TmpString),
    re_replace("mem", "", TmpString, Tmp2String),
    re_replace("}", "]", Tmp2String, String).

iterate_over_list([], _).

iterate_over_list([Elem], LatexFile) :-
    tree_to_latex(Elem, LatexFile).

iterate_over_list([Elem | List], LatexFile) :-
    iterate_over_list(List, LatexFile),
    write(LatexFile, "\n&\n"),
    tree_to_latex(Elem, LatexFile).
