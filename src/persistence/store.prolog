% this file contains the interface to store the
% derivation tree to the disk.
%
% Author: Vincenzo Palazzo <vincenzopalazzodev@gmail.com>

:- consult("latex.prolog").

% store/3
store("latex", DerivationTree, Where, Width) :-
    store_to_latex(Where, DerivationTree, Width).
